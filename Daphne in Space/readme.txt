DDD	 AA	PPPP	H  H	N   N	EEEE		SSSS	PPPP	 AA	CCCC	EEEE	!!
D  D	A  A	P  P	H  H	NN  N	E		S	P  P	A  A	C	E	!!
D  D	AAAA	PPPP	HHHH	N N N	EEE	IN	SSSS	PPPP	AAAA	C	EEEE	!!
D  D	A  A	P	H  H	N  NN	E		   S	P	A  A	C	E	
DDD	A  A	P	H  H	N   N	EEEE		SSSS	P	A  A	CCCC	EEEE  	!!

http://www.moosader.com/

Made Dec 18th, 2007 (in one night)
Last updated Sept 21, 2008

--------------------------------------
!!!!!!		CONTROLS	!!!!!!
--------------------------------------

* WASD or arrow keys to move
* F or Enter to use bullet-shield
* Spacebar or right CTRL to shoot
* Esc to pause
* F4 to quit
* F5 to toggle fullscreen


--------------------------------------
!!!!!!		GIFTINGS	!!!!!!
--------------------------------------

* To Daphne (da - FlyingMarshmallows) 
for no real reason except that I was bored

--------------------------------------
!!!!!!		CREDITS		!!!!!!
--------------------------------------

* Programming, Graphics, and Music by Rachel J. Morris
Music written with Musagi
Sound effects generated with SFXR